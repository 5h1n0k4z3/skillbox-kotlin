package com.skillbox.skillboxtest

val firstName = "Kirill"
val lastName = "Moskalew"
var height = 186
val weight = 81f
var isChild = checkAge()
var info = about()
fun main() {
    println(info)
    updateHeight(60)
    println(info)
}
fun checkAge() = height < 150 || weight < 40
fun about() = """
My fullName is $firstName $lastName. I am ${height}cm tall
 and ${weight}kg wieight. I am ${if(isChild) "Child" else "Adult"} now!
"""
fun updateHeight(setHeight: Int) {
    height = setHeight;
    isChild = checkAge()
    info = about()
}
